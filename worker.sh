#!/usr/bin/env bash

dockerName="docker-popov-photo"

Bred="\033[37;1;41m"
Tred="\033[1;31m"
Bgreen="\033[37;1;42m"
Tgreen="\033[32m"
Bblue="\033[37;1;44m"
Tyellow="\033[33m"
Tsea="\033[36m"
Reset="\033[0m \033[97m"
ResetAll="\033[0m"

echo -e "$Reset"

function quit {
    separate
    echo -e "$ResetAll"

    exit 0
}

function separate {
    echo "+--------------------------------------------------------------------------------------------------+"
}

if [ "$1" = "refresh" ]; then

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Updating Apache configuration"
    separate

    Custom=$(<docker/000-default.conf)
    sudo docker exec -it $dockerName sh -c "rm /etc/apache2/sites-available/000-default.conf"
    sudo docker exec -it $dockerName sh -c "touch /etc/apache2/sites-available/000-default.conf"
    sudo docker exec -it $dockerName sh -c "echo '$Custom' > /etc/apache2/sites-available/000-default.conf"

    quit
fi

if [ "$1" = "userset" ]; then

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Creating MYSQL user..."
    separate

    sudo docker exec -it $dockerName sh -c "mysql -uroot -e \"GRANT ALL PRIVILEGES ON *.* TO 'iamuser'@'localhost' IDENTIFIED BY 'iampassword';\""

    quit
fi

if [ "$1" = "reset" ]; then
    sudo docker system prune -a

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Stoping container..."
    separate

    echo -e "$Tsea"
    sudo docker stop $dockerName
    echo -e "$Reset"

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Remove container..."
    separate

    echo -e "$Tsea"
    sudo docker container rm $dockerName
    echo -e "$Reset"

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Remove image..."
    separate

    echo -e "$Tsea"
    sudo docker rmi -f $dockerName
    echo -e "$Reset"

    quit
fi

if [ "$1" = "connect" ]; then

    separate

    if ! sudo docker ps | grep '$dockerName' > /dev/null 2>&1; then

        printf "|$Tyellow%-97s$Reset|\n" " Docker is not running. Run docker automatically..."
        separate
        sudo docker start $dockerName
        separate
    fi


    printf "|$Tyellow%-97s$Reset|\n" " Connect to server..."
    separate

    echo -e "$ResetAll"
    sudo docker exec -it $dockerName /bin/bash
    echo -e "$Reset"

    quit
fi


if [ "$1" = "stop" ]; then

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Stoping Docker..."
    separate

    echo -e "$ResetAll"
    sudo docker stop $dockerName
    echo -e "$Reset"

    quit
fi


separate
printf "|$Tyellow%-97s$Reset|\n" " Start process installation"
separate

# Check docker installation
if docker -v | grep 'Docker version' > /dev/null 2>&1; then

	printf "|%-48s|$Tgreen%48s$Reset|\n" " Check Docker" "OK "
else

    printf "|%-48s|$Tred%48s$Reset|\n" " Check Docker" "Docker not found. Please, install Docker. "
    quit
fi



separate
if sudo docker images | grep 'docker-popov-photo' > /dev/null 2>&1; then

	printf "|%-48s|$Tgreen%48s$Reset|\n" " Check image for Photo project" "OK "

else
    printf "|%-48s|$Tred%48s$Reset|\n" " Check image for Photo project" "Image not found "
    separate
    printf "|$Tyellow%-97s$Reset|\n" " Building of image"
    separate
    sleep 2

    echo -e "$Tsea"
    sudo docker build -t docker-popov-photo .
    sudo docker run -d --name docker-popov-photo -p "2358:80" -p "2359:3306" -v ${PWD}/app:/app mattrayner/lamp:latest-1604-php7

    sudo docker exec -it $dockerName sh -c "apt-get -y update && apt-get install htop && apt-get install nano"

    Custom=$(<docker/000-default.conf)
    sudo docker exec -it $dockerName sh -c "rm /etc/apache2/sites-available/000-default.conf"
    sudo docker exec -it $dockerName sh -c "touch /etc/apache2/sites-available/000-default.conf"
    sudo docker exec -it $dockerName sh -c "echo '$Custom' > /etc/apache2/sites-available/000-default.conf"


    if ! sudo docker ps | grep '$dockerName' > /dev/null 2>&1; then
        echo -e "$Reset"
        separate
        printf "|$Tyellow%-97s$Reset|\n" " Docker is not running. Run docker automatically..."
        separate
        echo -e "$Tsea"

        sudo docker start $dockerName
    fi

    echo -e "$Reset"
    separate
    printf "|$Tyellow%-97s$Reset|\n" " Installation config of MYSQL. Need wait few minutes..."
    separate
    echo -e "$Tsea"

    sleep 240

    sudo docker exec -it $dockerName sh -c "mysql -uroot -e \"GRANT ALL PRIVILEGES ON *.* TO 'iamuser'@'localhost' IDENTIFIED BY 'iampassword';\""


    echo -e "$Reset"

fi



quit