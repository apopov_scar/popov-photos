<?php
/**
 * Created by Popov Aleksey.
 * User: scar
 * Mail: popov.scar@gmail.com
 * Date: 10.01.19
 * Time: 15:35
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class DefaultController
{
    public function index()
    {
        $number = mt_rand(0, 100);

        return new Response(
            '<html><body>Lucky fucking number: ' . $number . '</body></html>'
        );
    }
}